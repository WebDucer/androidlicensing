/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.app.services;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Data structure for IPC containing the server response forwarded by the
 * AppCenter.
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
public class SignedResponse implements Parcelable
{

    private final String mIsLicensed;
    private final String mSign;
    private final String mSignedData;

    private final static String NULL = ""; // provide empty string as NULL to
                                           // ensure proper marshaling

    public static final Parcelable.Creator<SignedResponse> CREATOR = new Parcelable.Creator<SignedResponse>()
    {

        @Override
        public SignedResponse createFromParcel(Parcel in)
        {
            String isLicensed = in.readString();
            String sign = in.readString();
            String signedData = in.readString();
            return new SignedResponse(isLicensed, sign, signedData);
        }

        @Override
        public SignedResponse[] newArray(int size)
        {
            return new SignedResponse[size];
        }
    };

    SignedResponse(String isLicensed, String sign, String signedData)
    {
        mIsLicensed = isLicensed;
        mSign = (sign == null) ? NULL : sign;
        mSignedData = (signedData == null) ? NULL : signedData;
    }

    /**
     * @see android.os.Parcelable#describeContents()
     */
    @Override
    public int describeContents()
    {
        return 0;
    }

    /**
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(mIsLicensed);
        out.writeString(mSign);
        out.writeString(mSignedData);
    }

    /**
     * The response code from the license server.
     * 
     * @return the isLicensed message from the server
     */
    public String getIsLicensed()
    {
        return mIsLicensed;
    }

    /**
     * The signature to the signed data that was created on the license server.
     * 
     * @return the sign string from the server response
     */
    public String getSign()
    {
        return mSign;
    }

    /**
     * The signed data is a "pipe"-separated string which contains the apps
     * package name, the response code, the salt and the user id in that order:
     * "com.pkg.app|LICENSED|1234567890|9876543210"
     * 
     * @return the string representation of the signed data from the server
     *         response
     */
    public String getSignedData()
    {
        return mSignedData;
    }
}
