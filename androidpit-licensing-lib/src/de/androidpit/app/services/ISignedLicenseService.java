/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/carsten/workspace/androidpit-licensing-lib/src/de/androidpit/app/services/ISignedLicenseService.aidl
 */
package de.androidpit.app.services;
public interface ISignedLicenseService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements de.androidpit.app.services.ISignedLicenseService
{
private static final java.lang.String DESCRIPTOR = "de.androidpit.app.services.ISignedLicenseService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an de.androidpit.app.services.ISignedLicenseService interface,
 * generating a proxy if needed.
 */
public static de.androidpit.app.services.ISignedLicenseService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = (android.os.IInterface)obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof de.androidpit.app.services.ISignedLicenseService))) {
return ((de.androidpit.app.services.ISignedLicenseService)iin);
}
return new de.androidpit.app.services.ISignedLicenseService.Stub.Proxy(obj);
}
public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_isLicensed:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
int _arg2;
_arg2 = data.readInt();
boolean _arg3;
_arg3 = (0!=data.readInt());
de.androidpit.app.services.SignedResponse _result = this.isLicensed(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_authenticate:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
boolean _arg2;
_arg2 = (0!=data.readInt());
this.authenticate(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements de.androidpit.app.services.ISignedLicenseService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
   * Signed Request for an existing license for the App with the associated
   * public key of the developer.
   *
   * @param publicKey
   *         Base64 encoded public key of the developer
   * @param pname
   *         Package name of the app
   * @param salt
   *         SALT for signature created server-side
   * @param debug
   *         Flag to turn off the retry cache
   *
   * @returns a signed response with the error code. See API documentation.   
   */
public de.androidpit.app.services.SignedResponse isLicensed(java.lang.String publicKey, java.lang.String pname, int salt, boolean debug) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
de.androidpit.app.services.SignedResponse _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(publicKey);
_data.writeString(pname);
_data.writeInt(salt);
_data.writeInt(((debug)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_isLicensed, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = de.androidpit.app.services.SignedResponse.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
   * Authenticates User of the the App against the AppCenter.
   * 
   * @param emailAddress
   *         E-mail address to identify the user
   * @param password
   *         The password of the user
   * @param remember
   *         Flag to remember the data in the App Center
   */
public void authenticate(java.lang.String emailAddress, java.lang.String password, boolean remember) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(emailAddress);
_data.writeString(password);
_data.writeInt(((remember)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_authenticate, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_isLicensed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_authenticate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
}
/**
   * Signed Request for an existing license for the App with the associated
   * public key of the developer.
   *
   * @param publicKey
   *         Base64 encoded public key of the developer
   * @param pname
   *         Package name of the app
   * @param salt
   *         SALT for signature created server-side
   * @param debug
   *         Flag to turn off the retry cache
   *
   * @returns a signed response with the error code. See API documentation.   
   */
public de.androidpit.app.services.SignedResponse isLicensed(java.lang.String publicKey, java.lang.String pname, int salt, boolean debug) throws android.os.RemoteException;
/**
   * Authenticates User of the the App against the AppCenter.
   * 
   * @param emailAddress
   *         E-mail address to identify the user
   * @param password
   *         The password of the user
   * @param remember
   *         Flag to remember the data in the App Center
   */
public void authenticate(java.lang.String emailAddress, java.lang.String password, boolean remember) throws android.os.RemoteException;
}
