/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

/**
 * Enum to provide status codes from the AndroidPIT license server, the
 * AppCenter and the Google servers.
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
public enum AndroidPitLicenseCheckCode {

    // ------------ SERVER MESSAGES ------------------------------ //
    /**
     * Licensing was successful. This is not realy an error but a positive
     * notification
     */
    LICENSED,

    /**
     * The licensing request was successful but denied
     * */
    NOT_LICENSED,

    /**
     * There is no license for that App on AndroidPIT
     */
    ERROR_NOT_APP_CENTER_MANAGED,

    /**
     * The package name in the list of Apps at AndroidPIT
     */
    ERROR_INVALID_PACKAGE_NAME,

    /**
     * The message could not be decrypted.
     */
    ERROR_DECRYPTION_ERROR,

    // ------------ APPCENTER MESSAGES ------------------------------ //
    /**
     * The user is not authenticated through the AndroidPIT AppCenter
     */
    ERROR_NOT_AUTHENTICATED,

    /**
     * The provided public key is not valid.
     */
    ERROR_INVALID_PUBLIC_KEY,

    /**
     * The message could not be encrypted.
     */
    ERROR_ENCRYPTION_ERROR,

    /**
     * Some internal server errors occured
     */
    ERROR_SERVER_FAILURE,

    /**
     * Handheld is not connected to the Internet
     */
    ERROR_NOT_CONNECTED,

    // ------------ LIBRARY MESSAGES ------------------------------ //
    /**
     * The communication with the AndroidPIT AppCenter failed. AppCenter is
     * possibly not installed.
     */
    ERROR_COMMUNICATING_WITH_APPCENTER_0111,

    ERROR_COMMUNICATING_WITH_APPCENTER_0112,

    ERROR_COMMUNICATING_WITH_APPCENTER_0113,

    ERROR_COMMUNICATING_WITH_APPCENTER_0121,

    ERROR_COMMUNICATING_WITH_APPCENTER_0211,

    ERROR_COMMUNICATING_WITH_APPCENTER_0212,

    ERROR_COMMUNICATING_WITH_APPCENTER_0213,

    ERROR_COMMUNICATING_WITH_APPCENTER_0221,

    /**
     * Unknown error found
     */
    ERROR_UNKNOWN,

    // All states below match to the google errors noted in their API docu,
    // except that the name is prepended with a "GOOGLE_".
    GOOGLE_INVALID_PACKAGE_NAME, GOOGLE_NON_MATCHING_UID, GOOGLE_NOT_MARKET_MANAGED, GOOGLE_CHECK_IN_PROGRESS, GOOGLE_INVALID_PUBLIC_KEY, GOOGLE_MISSING_PERMISSION;

    /**
     * Converts a string to an AndroidPitLicenseCheckCode. Returns ERROR_UNKNOWN
     * if the string is not known.
     * 
     * @param value
     *            the string value
     * 
     * @return the enum
     */
    public static AndroidPitLicenseCheckCode fromString(final String value)
    {
        try
        {
            return AndroidPitLicenseCheckCode.valueOf(value);
        }
        catch (IllegalArgumentException ex)
        {
            return AndroidPitLicenseCheckCode.ERROR_UNKNOWN;
        }
    }

}
