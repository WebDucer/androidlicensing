/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

/**
 * Callback interface for the license check. It is injected from the App to
 * allow asynchronous notifications about license success or failure and the
 * error code.
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
public interface IAndroidPitLicenseCheckerCallback
{

    /**
     * The user has a valid license. Grant access.
     */
    public void allow();

    /**
     * The user does not have a valid license. Show an appropriate message.
     */
    public void dontAllow();

    /**
     * An error occurred when checking for a valid license.
     * 
     * @param errorCode
     *            a pseudo enumeration of possible errors. @see
     *            de.androidpit.licensing.AndroidPitLicenseCheckError for
     *            details
     */
    public void applicationError(AndroidPitLicenseCheckCode errorCode);

}
