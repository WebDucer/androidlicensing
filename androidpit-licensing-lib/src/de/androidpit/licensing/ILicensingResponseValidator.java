/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

import de.androidpit.app.services.SignedResponse;

/**
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
interface ILicensingResponseValidator
{
    /**
     * A salt to make the signed data more unpredictable, e.g. a random number.
     * Do not forget to remember thes salt from the request at least until you
     * have received the response and validated the salt.
     * 
     * @return the salt as an integer
     */
    int getSalt();

    /**
     * Checks the response on validity if it comes from the server and extract
     * the response code. If the validation of the signed repsonse failes. It
     * has to return {@link AndroidPitLicenseCheckCode}.NOT_LICENSED
     * 
     * @see AndroidPitResponseData for the expected structure of the signedData
     *      field in SignedResponse
     * 
     * @param publicKey
     *            the public key of the developer, provided by the app.
     * @param response
     *            the server response forwarded by the AppCenter.
     * 
     * @return the response code of the AppCenter.
     */
    AndroidPitLicenseCheckCode checkResponse(
            String publicKey,
            SignedResponse response);
}
