/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

import java.util.regex.Pattern;

import android.text.TextUtils;

/**
 * Container for the response data from the server.
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
public class AndroidPitResponseData
{
    String mPackageName;
    String mResponseCode;
    int mSalt = 0;
    int mUserId = 0;

    /**
     * Creates an AndroidPitResponseData object based on the response string
     * from the sever. The data is a "pipe"-separated string which contains the
     * apps package name, the response code, the salt and the user id in that
     * order: "com.my.pkg|LICENSED|1234567890|9876543210"
     * 
     * @param responseDataStr
     *            the response string.
     * 
     * @return a filled object with all response data
     */
    public static AndroidPitResponseData parse(String responseDataStr)
    {
        AndroidPitResponseData responseData = new AndroidPitResponseData();
        try
        {

            String[] data = TextUtils
                    .split(responseDataStr, Pattern.quote("|"));
            if (data.length == 4)
            {
                responseData.mPackageName = data[0];
                responseData.mResponseCode = data[1];
                responseData.mSalt = Integer.parseInt(data[2]);
                responseData.mUserId = Integer.parseInt(data[3]);
                return responseData;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /**
     * Compares this response data against given credetials.
     * 
     * @param packageName
     *            package name of the app the library is linked against.
     * @param responseCode
     *            the response code received from the AppCenter.
     * @param salt
     *            the local store of the salt that was sent to the server.
     * 
     * @return true if they validate against each other, false else.
     */
    public boolean verifiesAgainst(
            String packageName,
            String responseCode,
            int salt)
    {
        if (mResponseCode == null || !mResponseCode.equals(responseCode))
        {
            return false;
        }

        if (mPackageName == null || !mPackageName.equals(packageName))
        {
            return false;
        }

        if (mSalt == 0 || mSalt != salt)
        {
            return false;
        }

        if (mUserId == 0)
        {
            return false;
        }

        return true;
    }

}
