/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;

import de.androidpit.app.services.ILicenseService;

/**
 * The AndroidPitLicenseChecker is a delegator to check for existing licensing
 * of the User against Google and the AndroidPIT AppCenter.
 * 
 * Using against the AndroidPIT license server is as simple as this:
 * 
 * <pre>
 * IAndroidPitLicenseCheckerCallback licenseCheckerCallback = new MyLicenseCheckerCallback();
 * AndroidPitLicenseChecker checker = new AndroidPitLicenseChecker(
 *         this,
 *         getPackageName(),
 *         ANDROIDPIT_PUBLIC_KEY);
 * checker.checkAccess(mLicenseCheckerCallback);
 * </pre>
 * 
 * If you want to check against the Google license system too, modify the
 * constructor:
 * 
 * <pre>
 * IAndroidPitLicenseCheckerCallback licenseCheckerCallback = new MyLicenseCheckerCallback();
 * AndroidPitLicenseChecker checker = new AndroidPitLicenseChecker(
 *         this,
 *         getPackageName(),
 *         ANDROIDPIT_PUBLIC_KEY,
 *         myGoogleLicensePolicy,
 *         GOOGLE_LICENSE_PUBLIC_KEY);
 * checker.checkAccess(mLicenseCheckerCallback);
 * </pre>
 * 
 * For details of the Google license policy have a look at the market licensing
 * in the android development docs.
 * 
 * For more in depth information have a look at our App Licensing How-To page at
 * 
 * {@link http://www.androidpit.com/en/android/developer-licensing-howto}
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */

public final class AndroidPitLicenseChecker
{

    private final static String ANDROID_PIT_LICENSING_INTENT = "de.androidpit.app.services.ILicenseService";

    private static final String LOG_TAG = "AndroidPitLicenseChecker";

    Context mContext;

    final String mLicenseServiceMutex = "mutex";
    final String mAppPkgName;
    final String mAndroidPitPublicKey;

    CheckLicenseThread mCheckLicenseThread;
    ILicenseService mAndroidPitLicenseService;
    IAndroidPitLicenseCheckerCallback mAndroidPitLicenseCheckerCallback;

    boolean mDebug = false;
    boolean mAutoDestroyAfterCheck = false;

    /**
     * Creates the AndroidPitLicenseChecker only with the data to check against
     * the AndroidPIT AppCenter.
     * 
     * @param context
     *            The context of the application.
     * @param appPkgName
     *            The package id of the app.
     * @param androidPitPublicKey
     *            The developers public key at AndroidPIT
     */
    public AndroidPitLicenseChecker(
            final Context context,
            final String appPkgName,
            final String androidPitPublicKey)
    {
        mContext = context;
        mAppPkgName = appPkgName;

        // Some developers put new lines into the constant
        mAndroidPitPublicKey = androidPitPublicKey.replace("\n", "").replace(
                "\r",
                "");

        Log.i(LOG_TAG, "AndroidPitLicenseChecker created (1)");
    }

    private LicenseChecker mGoogleLicenseChecker;
    private GoogleLicenseCheckerCallback mGoogleLicenseCheckerCallback;

    /**
     * Creates the AndroidPitLicenseChecker with all data to check against the
     * AndroidPIT AppCenter and Google. Google is checked first. If it fails
     * AndroidPIT AppCenter will be consulted.
     * 
     * @param context
     *            The context of the application.
     * @param appPkgName
     *            The package id of the app.
     * @param androidPitPublicKey
     *            The developers public key at AndroidPIT
     * @param googlePolicy
     *            Policy for Google licensing. @see Google API documentation
     * @param googlePublicKey
     *            Public key for Google licensing. @see Google API documentation
     */
    public AndroidPitLicenseChecker(
            final Context context,
            final String appPkgName,
            final String androidPitPublicKey,
            final Policy googlePolicy,
            final String googlePublicKey)
    {
        mContext = context;
        mGoogleLicenseChecker = new LicenseChecker(
                context,
                googlePolicy,
                googlePublicKey);
        mGoogleLicenseCheckerCallback = new GoogleLicenseCheckerCallback();
        mAppPkgName = appPkgName;

        // Some developers put new lines into the constant
        mAndroidPitPublicKey = androidPitPublicKey.replace("\n", "").replace(
                "\r",
                "");

        Log.i(LOG_TAG, "AndroidPitLicenseChecker created (2)");
    }

    /**
     * Checks if the User has access to the App.
     * 
     * @param callback
     *            callback interface from the App to notify about success or
     *            failure.
     */
    public void checkAccess(final IAndroidPitLicenseCheckerCallback callback)
    {
        if (mContext == null)
        {
            throw new RuntimeException("You've already called onDestroy().");
        }

        mAndroidPitLicenseCheckerCallback = callback;
        if (mGoogleLicenseChecker != null)
        {
            Log.i(LOG_TAG, "checkAccess invoked; "
                    + "delegating to Google's checkAccess...");
            mGoogleLicenseChecker.checkAccess(mGoogleLicenseCheckerCallback);
        }
        else
        {
            Log.i(LOG_TAG, "checkAccess invoked; checking with AndroidPIT...");
            androidPitDoCheck();
        }
    }

    protected void androidPitDoCheck()
    {
        mCheckLicenseThread = new CheckLicenseThread();
        mCheckLicenseThread.start();
    }

    /**
     * Sets debugging on or of. This mainly enables or disables the caching of
     * the license state. BEWARE! Enabling this will produce high load on our
     * systems!
     * 
     * @param debug
     */
    public void setDebug(boolean debug)
    {
        mDebug = debug;
    }

    /**
     * If you set this to true, the onDestroy method will be called
     * automatically after the response listener has been notified.
     */
    public void setAutoDestroyAfterCheck(boolean autoDestroyAfterCheck)
    {
        this.mAutoDestroyAfterCheck = autoDestroyAfterCheck;
    }

    /**
     * "Destructor" to clean up the application heap.
     */
    public void onDestroy()
    {
        Log.i(LOG_TAG, "Cleaning up...");

        if (mGoogleLicenseChecker != null)
        {
            mGoogleLicenseChecker.onDestroy();
            mGoogleLicenseChecker = null;
        }

        mAndroidPitLicenseService = null;
        mCheckLicenseThread = null;
        mAndroidPitLicenseCheckerCallback = null;
        mContext = null;
    }

    /**
     * Sends an application error to the callback listener.
     * 
     * @param code
     *            the error code
     * @param msg
     *            an additional error message to be logged
     * @param ex
     *            an optional exception to be logged
     */
    void applicationError(
            AndroidPitLicenseCheckCode code,
            String msg,
            Exception ex)
    {
        Log.i(LOG_TAG, "applicationError invoked; code = " + code + ";  msg = "
                + msg);

        if (msg != null)
        {
            if (ex != null)
            {
                Log.e(LOG_TAG, msg, ex);
            }
            else
            {
                Log.e(LOG_TAG, msg);
            }
        }

        mAndroidPitLicenseCheckerCallback.applicationError(code);

        if (mAutoDestroyAfterCheck)
        {
            onDestroy();
        }
    }

    class CheckLicenseThread extends Thread implements OnClickListener,
            ServiceConnection
    {

        protected View mLoginDialogView;

        @Override
        public void run()
        {
            Log.i(LOG_TAG, "Binding to App Center...");

            Intent licenseIntent = new Intent(ANDROID_PIT_LICENSING_INTENT);

            if (mContext.bindService(
                    licenseIntent,
                    this,
                    Context.BIND_AUTO_CREATE))
            {
                checkLicense();
            }
            else
            {
                applicationError(
                        AndroidPitLicenseCheckCode.ERROR_COMMUNICATING_WITH_APPCENTER_0111,
                        "Failed waiting for binding to App Center License Service",
                        null);

                if (mAutoDestroyAfterCheck)
                {
                    onDestroy();
                }
            }
        }

        private void checkLicense()
        {
            Log.i(LOG_TAG, "Checking license...");

            if (mAndroidPitLicenseService == null)
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch (InterruptedException ex)
                {
                    // Unbind must be called before any callback method,
                    // in which the developers usually call onDestroy
                    mContext.unbindService(this);

                    applicationError(
                            AndroidPitLicenseCheckCode.ERROR_COMMUNICATING_WITH_APPCENTER_0112,
                            "Failed waiting for binding to App Center License Service",
                            ex);

                    return;
                }
            }

            AndroidPitLicenseCheckCode apLicensed = AndroidPitLicenseCheckCode.ERROR_UNKNOWN;
            try
            {
                apLicensed = AndroidPitLicenseCheckCode
                        .fromString(mAndroidPitLicenseService.isLicensed(
                                mAndroidPitPublicKey,
                                mAppPkgName,
                                mDebug));
            }

            // Whenever an unknown error occurred in the remote service,
            // the client throws an NPE
            catch (NullPointerException ex)
            {
                // Unbind must be called before any callback method, in
                // which the developers usually call onDestroy
                mContext.unbindService(this);

                applicationError(
                        AndroidPitLicenseCheckCode.ERROR_COMMUNICATING_WITH_APPCENTER_0113,
                        "NPE while binding to App Center License Service",
                        ex);

                return;
            }

            catch (RemoteException ex)
            {
                // Unbind must be called before any callback method, in which
                // the developers usually call onDestroy
                mContext.unbindService(this);

                applicationError(
                        AndroidPitLicenseCheckCode.ERROR_COMMUNICATING_WITH_APPCENTER_0121,
                        "Unable to access AndroidPIT App Center to check license.",
                        ex);

                return;
            }

            Log.i(LOG_TAG, "License check returned " + apLicensed);

            // We check first for ERROR_NOT_AUTHENTICATED, because that's the
            // only case where we don't clean up unconditionally.
            if (AndroidPitLicenseCheckCode.ERROR_NOT_AUTHENTICATED
                    .equals(apLicensed))
            {
                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.post(new Runnable()
                {

                    @Override
                    public void run()
                    {
                        AlertDialog.Builder alert = new AlertDialog.Builder(
                                mContext);

                        alert.setTitle(R.string.login_dialog_title);

                        mLoginDialogView = View.inflate(
                                mContext,
                                R.layout.authenticate,
                                null);
                        alert.setView(mLoginDialogView);

                        alert.setPositiveButton(
                                R.string.login_button_login,
                                CheckLicenseThread.this);
                        alert.setNegativeButton(
                                R.string.login_button_cancel,
                                CheckLicenseThread.this);

                        // If the context is not an activity,
                        // this will throw a BadTokenException
                        try
                        {
                            alert.show();
                        }
                        catch (Exception ex)
                        {
                            // Unbind must be called before any callback
                            // method, in which the developers usually call
                            // onDestroy
                            mContext.unbindService(CheckLicenseThread.this);

                            applicationError(
                                    AndroidPitLicenseCheckCode.ERROR_NOT_AUTHENTICATED,
                                    "Could not show login dialog; returning ERROR_NOT_AUTHENTICATED",
                                    ex);
                        }
                    }
                });

                return;
            }

            // Unbind must be called *before* any callback method, in which
            // the developers usually call onDestroy
            mContext.unbindService(this);

            if (AndroidPitLicenseCheckCode.LICENSED.equals(apLicensed))
            {
                mAndroidPitLicenseCheckerCallback.allow();
            }

            else if (AndroidPitLicenseCheckCode.NOT_LICENSED.equals(apLicensed))
            {
                mAndroidPitLicenseCheckerCallback.dontAllow();
            }

            else
            {
                applicationError(apLicensed, null, null);
            }

            // onDestroy must be called *after* the callback methods
            if (mAutoDestroyAfterCheck)
            {
                onDestroy();
            }
        }

        @Override
        public synchronized void onServiceConnected(
                final ComponentName name,
                final IBinder service)
        {
            synchronized (mLicenseServiceMutex)
            {
                mAndroidPitLicenseService = ILicenseService.Stub
                        .asInterface(service);

                mLicenseServiceMutex.notifyAll();
            }
        }

        @Override
        public synchronized void onServiceDisconnected(final ComponentName name)
        {
            mAndroidPitLicenseService = null;
        }

        /**
         * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface,
         *      int)
         */
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            // Clicked on "Login"?
            if (which == DialogInterface.BUTTON_POSITIVE)
            {
                EditText editEmailAddress = (EditText) mLoginDialogView
                        .findViewById(R.id.editEmailAddress);
                EditText editPassword = (EditText) mLoginDialogView
                        .findViewById(R.id.editPassword);
                CheckBox checkBoxRemember = (CheckBox) mLoginDialogView
                        .findViewById(R.id.checkBoxRemember);

                String emailAddress = editEmailAddress.getText().toString();
                String password = editPassword.getText().toString();
                boolean remember = checkBoxRemember.isChecked();

                mLoginDialogView = null;

                try
                {
                    mAndroidPitLicenseService.authenticate(
                            emailAddress,
                            password,
                            remember);
                }
                catch (RemoteException ex)
                {
                    Log.e(LOG_TAG, "Unable to access AndroidPIT App Center"
                            + " to store login information.", ex);
                    return;
                }

                checkLicense();
            }

            // Canceled?
            else
            {
                // Unbind must be called before any callback method, in which
                // the developers usually call onDestroy
                mContext.unbindService(this);

                mLoginDialogView = null;

                applicationError(
                        AndroidPitLicenseCheckCode.ERROR_NOT_AUTHENTICATED,
                        null,
                        null);
            }
        }

    }

    class GoogleLicenseCheckerCallback implements LicenseCheckerCallback
    {

        /**
         * @see com.google.android.vending.licensing.LicenseCheckerCallback#allow(int)
         */
        @Override
        public void allow(int reason)
        {
            Log.i(LOG_TAG, "GoogleLicenseCheckerCallback.allow() invoked; "
                    + "reason = " + reason);

            mAndroidPitLicenseCheckerCallback.allow();

            if (mAutoDestroyAfterCheck)
            {
                onDestroy();
            }
        }

        /**
         * @see com.google.android.vending.licensing.LicenseCheckerCallback#dontAllow(int)
         */
        @Override
        public void dontAllow(int reason)
        {
            Log.i(LOG_TAG, "GoogleLicenseCheckerCallback.dontAllow() invoked; "
                    + "reason = " + reason
                    + "; now checking with AndroidPIT...");

            // Whenever Google says "dontAllow", check at AndroidPIT next.
            androidPitDoCheck();
        }

        /**
         * @see com.google.android.vending.licensing.LicenseCheckerCallback#applicationError(int)
         */
        @Override
        public void applicationError(int errorCode)
        {
            Log.i(LOG_TAG, "GoogleLicenseCheckerCallback.applicationError() "
                    + "invoked; errorCode = " + errorCode
                    + "; now checking with AndroidPIT...");

            // SW, 2011-09-13: After a Google error, check with AndroidPIT!!!
            androidPitDoCheck();
        }
    }

}
