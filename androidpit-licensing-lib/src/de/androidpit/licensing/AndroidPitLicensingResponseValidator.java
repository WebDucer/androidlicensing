/*
 *
 * Copyright (C) 2010-2012 Fonpit AG
 *
 */
package de.androidpit.licensing;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Random;

import android.util.Log;

import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;

import de.androidpit.app.services.SignedResponse;

/**
 * The implementation of the license validator which checks the validity of the
 * license response in-app.
 * 
 * @author <a href="mailto:carsten.rose@androidpit.de">Carsten W. Rose</a>
 */
final class AndroidPitLicensingResponseValidator implements
        ILicensingResponseValidator
{
    private final int mSalt;
    private final String mPackageName;

    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";

    /**
     * 
     */
    AndroidPitLicensingResponseValidator(String packageName)
    {
        mSalt = new Random().nextInt();
        mPackageName = packageName;
    }

    /**
     * @see de.androidpit.licensing.ILicensingResponseValidator#getSalt()
     */
    @Override
    public int getSalt()
    {
        return mSalt;
    }

    /**
     * @see de.androidpit.licensing.ILicensingResponseValidator#checkResponse(de.androidpit.app.services.SignedResponse)
     */
    @Override
    public AndroidPitLicenseCheckCode checkResponse(
            String publicKeyStr,
            SignedResponse response)
    {
        AndroidPitLicenseCheckCode responseCode = AndroidPitLicenseCheckCode
                .valueOf(response.getIsLicensed());

        // only check on positive license message
        if (responseCode.equals(AndroidPitLicenseCheckCode.LICENSED))
        {
            try
            {
                PublicKey pubKey = createPublicKey(publicKeyStr);
                Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
                sig.initVerify(pubKey);
                sig.update(response.getSignedData().getBytes());

                if (!sig.verify(Base64.decode(response.getSign())))
                {
                    Log.e(
                            this.getClass().getName(),
                            "failed to verify signature of response");
                    return AndroidPitLicenseCheckCode.NOT_LICENSED;
                }
            }
            catch (NoSuchAlgorithmException ex)
            {
                Log.e(this.getClass().getName(), "no such algorithm.");
                throw new RuntimeException(ex);
            }
            catch (InvalidKeyException ex)
            {
                Log.e(this.getClass().getName(), "bad public key.");
                return AndroidPitLicenseCheckCode.ERROR_INVALID_PUBLIC_KEY;
            }
            catch (SignatureException ex)
            {
                Log.e(this.getClass().getName(), "bad signature.");
                throw new RuntimeException(ex);
            }
            catch (Base64DecoderException ex)
            {
                Log.e(
                        this.getClass().getName(),
                        "failed decode base64 string of signature ("
                                + ex.getMessage() + ").");
                return AndroidPitLicenseCheckCode.NOT_LICENSED;
            }

            AndroidPitResponseData responseData = AndroidPitResponseData
                    .parse(response.getSignedData());

            if (responseData.verifiesAgainst(
                    mPackageName,
                    response.getIsLicensed(),
                    mSalt))
            {
                Log.e(this.getClass().getName(), "verification successful.");
                return responseCode;
            }
            else
            {
                Log.e(
                        this.getClass().getName(),
                        "signature verification failed - invalid data.");
                return AndroidPitLicenseCheckCode.NOT_LICENSED;
            }
        }
        else
        {
            // return AppCenter message
            return responseCode;
        }

    }

    /**
     * Converts the base64 encoded representation of a public key into a
     * PublicKey object.
     */
    private PublicKey createPublicKey(String pubKeyBase64)
    {
        try
        {
            // the pub key comes in as a Base64 coded string. Decode to the
            // byte array which contains the object stream of the public key
            ByteArrayInputStream pubKeyByteArray = new ByteArrayInputStream(
                    Base64.decode(pubKeyBase64));
            ObjectInputStream publicKeyObject = new ObjectInputStream(
                    pubKeyByteArray);
            BigInteger modulus = (BigInteger) publicKeyObject.readObject();
            BigInteger exponent = (BigInteger) publicKeyObject.readObject();

            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, exponent);
            KeyFactory rsaKeyFactory = KeyFactory.getInstance("RSA");
            return rsaKeyFactory.generatePublic(keySpec);
        }
        catch (Exception ex)
        {
            Log.e(
                    "LicenseResponseValidator",
                    "Deserialization of public key failed.",
                    ex);
            return null;
        }
    }

}
