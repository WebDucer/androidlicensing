package de.androidpit.app.services;

interface ILicenseService
{
  /**
   * Request for an existing license for the App with the associated
   * public key of the developer.
   *
   * @param publicKey
   *         Base64 encoded public key of the developer
   * @param pname
   *         Package name of the app
   * @param debug
   *         Flag to turn off the retry cache
   *
   * @returns a String with the error code. See API documentation.   
   */
  String isLicensed(in String publicKey, in String pname, boolean debug);

  /**
   * Authenticates User of the the App against the AppCenter.
   * 
   * @param emailAddress
   *         E-mail address to identify the user
   * @param password
   *         The password of the user
   * @param remember
   *         Flag to remember the data in the App Center
   */
  void authenticate(in String emailAddress, in String password, boolean remember);
}